# Age-appropriateness Classification of Czech Texts

## Assignment

This semester project focuses on developing a model for assessing the age-appropriateness of Czech-language texts, with particular emphasis on content intended for children aged 8-13 in educational contexts. While existing research primarily addresses content inappropriateness detection, such as toxicity [1], [2] or erotic content [3], this project takes a more comprehensive approach by incorporating additional crucial factors such as cognitive load and language complexity. The goal is to create a sophisticated model that evaluates the multifaceted aspects of text appropriateness beyond mere content filtering.

## Input Data

The training and evaluation data are sourced from the Czech National Corpus [4], which provides text samples categorized into two distinct classes:

* JUN: Literature specifically written for children
* GEN: Literature intended for the general public

The complete corpus comprises approximately 150,000 sentences labeled as JUN and 7.8 million sentences labeled as GEN. For experimental purposes, a balanced dataset containing 10,000 sentences from each category was created and is publicly available in the [project repository](https://gitlab.fit.cvut.cz/novott37/mvi-sp/-/tree/main/data?ref_type=heads). Data collection was performed through the [KonText Interface](https://www.korpus.cz/kontext/query?corpname=syn2015) using the following systematic pipeline:

1. Query: `.*` (retrieve all available data)
2. Constraint: `audience:JUN/GEN` (filter by age-appropriateness label)
3. Filter: `První nálezy ve větách` (eliminate duplicate entries)
4. Concordance: `Náhodný vzorek` (generate random samples)
5. Display: `KWIC/Věta` (sentence-level view)

The extracted data underwent preprocessing using a custom [cleaning script](https://gitlab.fit.cvut.cz/novott37/mvi-sp/-/blob/main/src/clean_corpus.py?ref_type=heads) to generate a normalized format with one sentence per line.

## Example Usage

```python
sys.path.append(os.path.join(os.getcwd(), "src"))

from transformers import RobertaForSequenceClassification, RobertaTokenizer
import subprocess
import sys
import os
from helpers import *

PATH_TO_DATA = "data/"
PATH_TO_RESULTS = "results/"

def load_data(file):
    with open(file + "-cleared.txt") as f:
        return f.read().strip().split("\n")[100:200]
        
positive_sentences = load_data(PATH_TO_DATA + "JUN-test")
negative_sentences = load_data(PATH_TO_DATA + "GEN-test")

all_sentences = positive_sentences + negative_sentences
labels = [1] * len(positive_sentences) + [0] * len(negative_sentences)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model_path = os.path.join(PATH_TO_RESULTS, "fine_tuned_model/")
tokenizer_path = os.path.join(PATH_TO_RESULTS + "fine_tuned_tokenizer/")
model = RobertaForSequenceClassification.from_pretrained(
    model_path,
    local_files_only=True
)

tokenizer = RobertaTokenizer.from_pretrained(
    tokenizer_path,
    local_files_only=True
)
model = model.to(device)

test_data = tokenize(tokenizer, all_sentences, labels)
test_dataset = SentenceDataset(test_data)
test_loader = DataLoader(test_dataset, batch_size=len(all_sentences))

results = evaluate_dataset(data_loader=test_loader, model=model, device=device)
visualize_evaluation_metrics(results, "", savefig=False)
```

---

**References:**
1. Barrientos, 2020
2. Jain, 2024
3. Merayo, 2019
4. Korpus, 2015
