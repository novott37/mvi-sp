from transformers import AutoTokenizer, AutoModel
from transformers import AutoModel
from transformers import ElectraForPreTraining, ElectraTokenizerFast
from transformers import Trainer, TrainingArguments
from transformers import RobertaForSequenceClassification, RobertaTokenizer
from transformers import EarlyStoppingCallback
import seaborn as sns
import matplotlib.pyplot as plt


from torch.utils.data import DataLoader, Dataset
from torch.optim import AdamW
import torch.nn as nn
import torch
from torch.nn import CrossEntropyLoss
from transformers import get_scheduler
from tqdm import tqdm

from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score, f1_score, accuracy_score, classification_report, confusion_matrix

import subprocess

from matplotlib.pyplot import plot, show, savefig
import seaborn as sns
from itertools import chain

from helpers import *

import csv

robe_model_name = "ufal/robeczech-base"
seznam_model_name = "Seznam/small-e-czech"

PATH_TO_DATA = "../data/"
PATH_TO_RESULTS = "../results/"

def load_data(file):
    with open(file + ".txt") as f:
        return f.read().strip().split("\n")
        
positive_sentences = load_data(PATH_TO_DATA + "JUN-test-cleared")
negative_sentences = load_data(PATH_TO_DATA + "GEN-test-cleared")

all_sentences = positive_sentences + negative_sentences
labels = [1] * len(positive_sentences) + [0] * len(negative_sentences)

train_indices, eval_indices = train_test_split(range(len(labels)), test_size=0.2, random_state=42)
valid_indices, test_indices = train_test_split(eval_indices, test_size=0.5, random_state=42)

train_sentences = [all_sentences[i] for i in train_indices]
valid_sentences = [all_sentences[i] for i in valid_indices]
test_sentences = [all_sentences[i] for i in test_indices]

train_labels = [labels[i] for i in train_indices]
valid_labels = [labels[i] for i in valid_indices]
test_labels = [labels[i] for i in test_indices]

tokenizer = RobertaTokenizer.from_pretrained("ufal/robeczech-base")

train_data = tokenize(tokenizer, train_sentences, train_labels)
valid_data = tokenize(tokenizer, valid_sentences, valid_labels)
test_data = tokenize(tokenizer, test_sentences, test_labels)

train_dataset = SentenceDataset(train_data)
valid_dataset = SentenceDataset(valid_data)
test_dataset = SentenceDataset(test_data)

train_loader = DataLoader(train_dataset, batch_size=16, shuffle=True)
valid_loader = DataLoader(valid_dataset, batch_size=16)
test_loader = DataLoader(test_dataset, batch_size=16)


model = RobertaForSequenceClassification.from_pretrained(
    "ufal/robeczech-base", 
    num_labels=2
)

model.config.hidden_dropout_prob = 0.3  # Dropout for fully connected layers
model.config.attention_probs_dropout_prob = 0.3  # Dropout for attention layers

optimizer_grouped_parameters = [
    {"params": model.roberta.encoder.layer[:6].parameters(), "lr": 1e-5},  # Lower layers
    {"params": model.roberta.encoder.layer[6:].parameters(), "lr": 3e-5},  # Higher layers
    {"params": model.classifier.parameters(), "lr": 5e-5},  # Classification head
]
optimizer = AdamW(optimizer_grouped_parameters, weight_decay=0.01)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model.to(device)

num_epochs = 20
num_training_steps = num_epochs * len(train_loader)
lr_scheduler = get_scheduler("linear", optimizer=optimizer, num_warmup_steps=0, num_training_steps=num_training_steps)

loss_fn = CrossEntropyLoss()

early_stopping_patience = 2
best_valid_loss = float('inf')
patience_counter = 0

for epoch in range(num_epochs):
    model.train()
    progress_bar = tqdm(train_loader, desc=f"Epoch {epoch + 1}")
    for batch in progress_bar:
        batch = {k: v.to(device) for k, v in batch.items()}
        
        outputs = model(**batch)
        loss = outputs.loss
        logits = outputs.logits

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        lr_scheduler.step()

        progress_bar.set_postfix(loss=loss.item())

    model.eval()
    valid_loss = 0
    with torch.no_grad():
        for batch in valid_loader:
            batch = {k: v.to(device) for k, v in batch.items()}
            outputs = model(**batch)
            valid_loss += loss_fn(outputs.logits, batch["labels"]).item()

    avg_valid_loss = valid_loss / len(valid_loader)
    print(f"Validation Loss: {valid_loss / len(valid_loader)}")
    
    if avg_valid_loss < best_valid_loss:
        best_valid_loss = avg_valid_loss
        patience_counter = 0
    else:
        patience_counter += 1
        if patience_counter >= early_stopping_patience:
            print("Early stopping triggered")
            break

model.save_pretrained(PATH_TO_RESULTS + "fine_tuned_model")
tokenizer.save_pretrained(PATH_TO_RESULTS + "fine_tuned_tokenizer")  

valid_results, test_results, y_pred_proba_valid, y_pred_proba_test = evaluate_classification(valid_loader, test_loader, model, device)

visualize_evaluation_metrics(valid_results, PATH_TO_RESULTS + "valid_metrics.pdf")
visualize_evaluation_metrics(test_results, PATH_TO_RESULTS + "test_metrics.pdf")

with open(PATH_TO_RESULTS + 'valid_probs.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(y_pred_proba_valid)
    
with open(PATH_TO_RESULTS + 'test_probs.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(y_pred_proba_test)