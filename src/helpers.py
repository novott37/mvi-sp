from transformers import AutoTokenizer, AutoModel
from transformers import AutoModel
from transformers import ElectraForPreTraining, ElectraTokenizerFast
from transformers import Trainer, TrainingArguments
from transformers import RobertaForSequenceClassification, RobertaTokenizer
from transformers import EarlyStoppingCallback
import seaborn as sns
import matplotlib.pyplot as plt


from torch.utils.data import DataLoader, Dataset
from torch.optim import AdamW
import torch.nn as nn
import torch
from torch.nn import CrossEntropyLoss
from transformers import get_scheduler
from tqdm import tqdm

from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score, f1_score, accuracy_score, classification_report, confusion_matrix

import subprocess

from matplotlib.pyplot import plot, show, savefig
import seaborn as sns
from itertools import chain

import numpy as np

# Set random seeds for reproducibility
def set_seed(seed_value=42):
    torch.manual_seed(seed_value)
    torch.cuda.manual_seed_all(seed_value)
    np.random.seed(seed_value)

def load_data(file):
    with open(file + "-cleared.txt") as f:
        return f.read().strip().split("\n")
        
# evaluate model from test data loader
def evaluate(data_loader: DataLoader, model: RobertaForSequenceClassification, device: torch.device):
    model.eval()
    predictions, probs, true_labels = [], [], []
    with torch.no_grad():
        for batch in tqdm(data_loader, desc="Testing"):
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['labels'].to(device)
            
            outputs = model(
                input_ids=input_ids,
                attention_mask=attention_mask,
                labels=labels
            )
            logits = outputs.logits
            predictions.extend(torch.argmax(logits, dim=-1).cpu().numpy())
            probs.extend(torch.softmax(logits, dim=-1).cpu().numpy()[:, 1])
            true_labels.extend(labels.cpu().numpy())

    return predictions, probs, true_labels

def evaluate_dataset(data_loader: DataLoader, model: RobertaForSequenceClassification, device: torch.device):
    y_pred_valid, y_pred_proba_valid, y_Valid = evaluate(data_loader, model, device)
    valid_results = {
        'accuracy': accuracy_score(y_Valid, y_pred_valid),
        'f1_score': f1_score(y_Valid, y_pred_valid),
        'confusion_matrix': confusion_matrix(y_Valid, y_pred_valid),
        'classification_report': classification_report(y_Valid, y_pred_valid),
        'roc_auc': roc_auc_score(y_Valid, y_pred_proba_valid)
    }
    return valid_results

def evaluate_classification(valid_loader: DataLoader, test_loader: DataLoader, model: RobertaForSequenceClassification, device: torch.device):
    y_pred_valid, y_pred_proba_valid, y_Valid = evaluate(valid_loader, model, device)
    y_pred_test, y_pred_proba_test, y_Test = evaluate(test_loader, model, device)

    valid_results = {
        'accuracy': accuracy_score(y_Valid, y_pred_valid),
        'f1_score': f1_score(y_Valid, y_pred_valid),
        'confusion_matrix': confusion_matrix(y_Valid, y_pred_valid),
        'classification_report': classification_report(y_Valid, y_pred_valid),
        'roc_auc': roc_auc_score(y_Valid, y_pred_proba_valid)
    }
    test_results = {
        'accuracy': accuracy_score(y_Test, y_pred_test),
        'f1_score': f1_score(y_Test, y_pred_test),
        'confusion_matrix': confusion_matrix(y_Test, y_pred_test),
        'classification_report': classification_report(y_Test, y_pred_test),
        'roc_auc': roc_auc_score(y_Test, y_pred_proba_test)
    }

    return valid_results, test_results, y_pred_proba_valid, y_pred_proba_test

def visualize_evaluation_metrics(results, output_file: str, savefig=True):
    confusion_matrix = results['confusion_matrix']
    accuracy = results['accuracy']
    f1 = results['f1_score']
    roc_auc = results['roc_auc']

    fig, axs = plt.subplots(1, 2, figsize=(16, 6))

    sns.heatmap(confusion_matrix, annot=True, fmt='d', cmap='Blues', cbar=False, ax=axs[0])
    axs[0].set_title('Confusion Matrix', fontsize=14)
    axs[0].set_xlabel('Predicted Label', fontsize=12)
    axs[0].set_ylabel('True Label', fontsize=12)

    metric_names = ['Accuracy', 'F1 Score', 'ROC-AUC']
    metric_values = [accuracy, f1, roc_auc]

    axs[1].barh(metric_names, metric_values, color=['skyblue', 'lightgreen', 'salmon'])
    axs[1].set_xlim(0, 1)
    axs[1].set_title('Metrics', fontsize=14)
    axs[1].set_xlabel('Score', fontsize=12)
    
    for i, v in enumerate(metric_values):
        axs[1].text(v + 0.02, i, f'{v:.2f}', va='center', fontsize=10)

    plt.tight_layout()
    if savefig:
        plt.savefig(output_file)
    else:
        plt.show()
    
def tokenize(tokenizer, sentences, labels):
    inputs = tokenizer(sentences, padding=True, truncation=True, return_tensors="pt", max_length=128)
    return {"input_ids": inputs["input_ids"], "attention_mask": inputs["attention_mask"], "labels": torch.tensor(labels)}

class SentenceDataset(Dataset):
    def __init__(self, data):
        self.input_ids = data["input_ids"]
        self.attention_mask = data["attention_mask"]
        self.labels = data["labels"]

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        return {
            "input_ids": self.input_ids[idx],
            "attention_mask": self.attention_mask[idx],
            "labels": self.labels[idx],
        }
