import torch
from torch.utils.data import DataLoader, TensorDataset
from transformers import RobertaForSequenceClassification, RobertaTokenizer
from transformers import AdamW, get_linear_schedule_with_warmup
from sklearn.model_selection import train_test_split
import numpy as np
from tqdm import tqdm

from helpers import *

robe_model_name = "ufal/robeczech-base"
PATH_TO_DATA = "../data/"
PATH_TO_RESULTS = "../results/"

class SentenceClassifier:
    def __init__(self, model_name=robe_model_name, num_labels=2):
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.tokenizer = RobertaTokenizer.from_pretrained(model_name)
        self.model = RobertaForSequenceClassification.from_pretrained(
            model_name,
            num_labels=num_labels
        ).to(self.device)
    
    def prepare_data(self, sentences, labels, max_length=128):
        encodings = self.tokenizer(
            sentences,
            truncation=True,
            padding=True,
            max_length=max_length,
            return_tensors="pt"
        )
        
        # Convert labels to tensor
        labels = torch.tensor(labels)
        
        return TensorDataset(
            encodings['input_ids'],
            encodings['attention_mask'],
            labels
        )
    
    def create_data_loaders(self, all_sentences, labels, batch_size=16):
        train_indices, eval_indices = train_test_split(
            range(len(labels)), 
            test_size=0.2, 
            random_state=42
        )
        valid_indices, test_indices = train_test_split(
            eval_indices,
            test_size=0.5,
            random_state=42
        )
        train_sentences = [all_sentences[i] for i in train_indices]
        valid_sentences = [all_sentences[i] for i in valid_indices]
        test_sentences = [all_sentences[i] for i in test_indices]
        
        train_labels = [labels[i] for i in train_indices]
        valid_labels = [labels[i] for i in valid_indices]
        test_labels = [labels[i] for i in test_indices]
        
        train_dataset = self.prepare_data(train_sentences, train_labels)
        valid_dataset = self.prepare_data(valid_sentences, valid_labels)
        test_dataset = self.prepare_data(test_sentences, test_labels)
        
        train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
        valid_loader = DataLoader(valid_dataset, batch_size=batch_size)
        test_loader = DataLoader(test_dataset, batch_size=batch_size)
        
        return train_loader, valid_loader, test_loader
    
    def train(self, train_loader, valid_loader, epochs=20):
        optimizer_grouped_parameters = [
            {"params": self.model.roberta.encoder.layer[:6].parameters(), "lr": 1e-5},  # Lower layers
            {"params": self.model.roberta.encoder.layer[6:].parameters(), "lr": 3e-5},  # Higher layers
            {"params": self.model.classifier.parameters(), "lr": 5e-5},  # Classification head
        ]
        optimizer = AdamW(optimizer_grouped_parameters, weight_decay=0.01)
        total_steps = len(train_loader) * epochs
        num_warmup_steps = int(0.01 * total_steps)
        scheduler = get_linear_schedule_with_warmup(
            optimizer,
            num_warmup_steps=num_warmup_steps,
            num_training_steps=total_steps
        )
        
        best_valid_loss = float('inf')
        
        for epoch in range(epochs):
            print(f"\nEpoch {epoch+1}/{epochs}")
            
            self.model.train()
            total_train_loss = 0
            
            for batch in tqdm(train_loader, desc="Training"):
                input_ids = batch[0].to(self.device)
                attention_mask = batch[1].to(self.device)
                labels = batch[2].to(self.device)
                
                self.model.zero_grad()
                
                outputs = self.model(
                    input_ids=input_ids,
                    attention_mask=attention_mask,
                    labels=labels
                )
                
                loss = outputs.loss
                total_train_loss += loss.item()
                
                loss.backward()
                torch.nn.utils.clip_grad_norm_(self.model.parameters(), 1.0)
                optimizer.step()
                scheduler.step()
            
            avg_train_loss = total_train_loss / len(train_loader)
            
            self.model.eval()
            total_valid_loss = 0
            
            with torch.no_grad():
                for batch in tqdm(valid_loader, desc="Validation"):
                    input_ids = batch[0].to(self.device)
                    attention_mask = batch[1].to(self.device)
                    labels = batch[2].to(self.device)
                    
                    outputs = self.model(
                        input_ids=input_ids,
                        attention_mask=attention_mask,
                        labels=labels
                    )
                    
                    total_valid_loss += outputs.loss.item()
            
            avg_valid_loss = total_valid_loss / len(valid_loader)
            
            print(f"\nAverage training loss: {avg_train_loss:.4f}")
            print(f"Average validation loss: {avg_valid_loss:.4f}")
            
            if avg_valid_loss < best_valid_loss:
                best_valid_loss = avg_valid_loss
                torch.save(self.model.state_dict(), 'best_model.pt')
                print("Saved best model!")
    
def main():
    set_seed()
    
    positive_sentences = load_data(PATH_TO_DATA + "JUN")[:100]
    negative_sentences = load_data(PATH_TO_DATA + "GEN")[:100]

    all_sentences = positive_sentences + negative_sentences
    labels = [1] * len(positive_sentences) + [0] * len(negative_sentences)

    classifier = SentenceClassifier()
    
    train_loader, valid_loader, test_loader = classifier.create_data_loaders(
        all_sentences,
        labels,
        batch_size=16
    )
    
    classifier.train(train_loader, valid_loader, epochs=20)

    valid_results, test_results = evaluate_classification(valid_loader, test_loader, classifier.model, classifier.device)
    visualize_evaluation_metrics(valid_results, PATH_TO_RESULTS + "valid_metrics.pdf")
    visualize_evaluation_metrics(test_results, PATH_TO_RESULTS + "test_metrics.pdf")

if __name__ == "__main__":
    main()
