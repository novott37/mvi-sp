# Input and output file paths
import argparse

def process_file(input_file, output_file):
    """Process the input file to extract and clean sentences."""
    with open(input_file, "r", encoding="utf-8") as infile, open(output_file, "w", encoding="utf-8") as outfile:
        for line in infile:
            parts = line.strip().split('|')
            if len(parts) != 2:
                continue
            
            sentence = parts[1].strip()
            
            if '<' in sentence and '>' in sentence:
                begin = sentence.index('<')
                end = sentence.index('>')
                sentence = (sentence[0:begin] + sentence[begin + 1:end] + sentence[end + 1:]).strip()
            
            outfile.write(sentence + "\n")

def main():
    parser = argparse.ArgumentParser(description="Clean sentences from a corpus file.")
    parser.add_argument("input_file", help="Path to the input file.")
    args = parser.parse_args()

    input_file = f"{args.input_file}.txt"
    output_file = f"{args.input_file}-cleared.txt"

    process_file(input_file, output_file)

if __name__ == "__main__":
    main()
